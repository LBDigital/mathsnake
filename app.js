'use strict'; // Run the script in strict mode


/* --- INITIAL APP CONFIGURATION --- */
const express = require('express'); // IMPORT the `express.js` module
const app = express(); // INSTANTIATE the express object as `app`
const http = require('http').Server(app); // IMPORT the `http` module

app.use(express.static(__dirname)); // Set the directory for static project files (.css/.js/.html)
/* --- */

/* --- Start the node.js server and listen for requests on port `1337` --- */
const server = app.listen(1337, function(){
	console.log("Listening on *:1337");
});
/* --- */

/* --- IMPORT REQUIRED MODULES --- */
// IMPORT `socket.io` and INSTANTIATE it on the server
const io = require('socket.io').listen(server, {'pingInterval': 4000, 'pingTimeout': 6000});
const fs = require('fs'); // `file-system` module allows the script to access and process local files
/* --- */


/* CONSTANTS */
const CONFIG = JSON.parse(fs.readFileSync('./config.json', 'utf-8'));
// DYNAMIC CONSTANTS (calculated based on other config values)
CONFIG.canvasSize = CONFIG.gridSize*20; // each cell is 20wx20h
CONFIG.gridMax = CONFIG.gridSize-1;
/* --- */



/* ROUTING */
app.get('*', function(req, res){ // catch all requests to the server ('*')
	switch (req.url) { // comparing the request url
		case '/': // if the root/index page is requested
			res.sendFile(__dirname + '/pages/index.html'); // display the main/home page to the user
			break;

		default: // runs if no cases were met
			res.sendFile(__dirname + '/pages/404.html'); // display a 404 error page to the user
	}
});
/* --- */



function newLog(logValue){ // log to the log.txt file, useful for network logging throughout the program
	fs.appendFile(__dirname + '/log.txt', logValue + "\n"); // write to file for more-permanent record
	console.log(logValue);
}




var GAME = {
	question: {}, // dictionary of info about current question

	setup: function(){ // called to start the main game loop
		this.mainLoop = setInterval(this.sendFrame, 1000/CONFIG.fps); // executes the 'sendFrame' method every (1000/fps) milliseconds
		GAME.newQuestion(); // generate and store a new question
	},

	sendFrame: function(){ // renders frame data, and sends to each connected user

		var renderSnakes = {}; // define an empty dictionary to store the alive snakes data
		for (var snakeId in snakes) { // for each snake that has been created
			if (snakes[snakeId].alive) { // only render snakes that are alive
				renderSnakes[snakeId] = snakes[snakeId]; // add the snake to the dictionary
			}
		}

		// a new frame only needs to be sent to client if atleast 1 alive snake exists
		if (Object.keys(renderSnakes).length > 0) {

			io.emit('renderFrame', renderSnakes, GAME.question); // emit the snakes data to the clients for rendering

			/* RECALCULATE SNAKE DATA for next frame */
			for (var snakeId in renderSnakes) { // for each snake that is being rendered
				var self = snakes[snakeId]; // save the loop-snake data as 'self' for ease of access
				for (var i = self.body.length-1; i>=0; i--) { // for each body part of the snake,

					var next;
					if (i>0) { // if this is a normal body part (not the head)
						next = {x:self.body[i-1].x, y:self.body[i-1].y}; // move this body part to the position of the next one along
					}else{ // i==0, so head/front of snake.
						switch (self.direction) { // switch through the direction, adjusting position accordingly.
							case 0: // direction is 0, so north
								snakes[snakeId].body[i].y -=1; // minus 1 from the y coordinate of the snakes head
								break;
							case 1: // direction is 1, so east
								snakes[snakeId].body[i].x +=1; // add 1 to the x coordinate of the snakes head
								break;
							case 2: // direction is 2, so south
								snakes[snakeId].body[i].y +=1; // add 1 to the y coordinate of the snakes head
								break;
							case 3: // direction is 3, so west
								snakes[snakeId].body[i].x -=1; // minus 1 from the x coordinate of the snakes head
								break;
						}
						next = { // save the position vector object for the new snake head position
							x: snakes[snakeId].body[i].x,
							y: snakes[snakeId].body[i].y
						};

						if (!GAME.coordValid(next)) { // if the new coordinate of the snakes head is invalid
							self.die(); // kill the snake
							return;
						}
						snakes[snakeId].eatFood(next); // check if food at next pos; and if so, eat it.
					}
					snakes[snakeId].body[i] = next; // update the snakes array to stored the new body part position

				}

			}
		}

	},

	// get a random position on the game grid
	getRandCoord: function(){
		var randX = parseInt(Math.random()*CONFIG.gridMax); // generate a random x coordinate in range 0,gridMax
		var randY = parseInt(Math.random()*CONFIG.gridMax); // generate a random y coordinate in range 0,gridMax

		return {x:randX,y:randY};
	},

	// check if a given coordinate is a valid position on the game grid
	coordValid: function(coord){
		if (coord.x < 0 || coord.x > CONFIG.gridMax) { // if the snake is off the grid horizontally
			console.log('OFFGRID');
			return false; // coordinate is invalid
		}else if (coord.y < 0 || coord.y > CONFIG.gridMax) { // if the snake is off the grid vertically
			console.log('OFFGRID');
			return false; // coordinate is invalid
		}

		// Check that the snake isn't coliding with any other snakes
		for (var snakeId in snakes) { // for each snake
			var self = snakes[snakeId];
			if (self.alive) { // if the loop-snake is alive
				for (var i = 1; i < self.body.length; i++) { // start from 1, not 0, so that doesn't check head.
					var bodyPart = self.body[i]; // get the bodypart
					if (bodyPart.x == coord.x && bodyPart.y == coord.y) { // if the bodyparts position collides with the given position
						console.log('COLLISION');
						return false; // coordinate is invalid
					}
				}
			}
		}

		return true; // hasn't returned false yet, so coordinate must be valid
	},

	// check if there is a food at the given coordinate, and execute callback with food data if there is
	foodAt: function(coord, callback){
		for (var colour in GAME.question.colours) { // for each question colour
			var locs = GAME.question.colours[colour].locs; // get all food locations for this colour
			for (var i = 0; i < locs.length; i++) { // loop through each location
				var loc = locs[i];
				// if this food items location matches given coord
				if (typeof loc !== "undefined" && loc.x == coord.x && loc.y == coord.y) {
					var food = { // setup a dictionary to store data about this food item
						'index':i,
						'loc': loc,
						'colour': colour
					}
					return callback(food); // return the data about the food item
				}
			}

		}
		return callback(false); // no food items were found at given coordinate
	},


	newQuestion: function(){ // generate a question
		var question = ""; // empty String to store the question
		var operators = ['+','-']; // possible operators
		question += randIntInRange(1,CONFIG.questionMax); // questionMax value set in config file
		question += operators[randIntInRange(0, operators.length-1)];
		question += randIntInRange(1,CONFIG.questionMax);
		var answer = eval(question); // evaluate the question string to get answer.

		var colours = CONFIG.colours; // fetch available answer colors from imported config file
		var answerColour = colours[randIntInRange(0,colours.length-1)]; // set the colour of the answer to a random colour

		var response = { // define the response dictionary for storing attributes about the new question
			question: question, // the raw string of the question
			answer: answer, // the evaluated answer
			answerColour: answerColour, // the colour that the answer is linked too
			colours: {} // define an empty dictionary to store the colours and their associated attributes (value, etc.)
		};

		var values = [answer]; // define an array that will be used to store used colour values
		var variance = CONFIG.colours.length; // set a variance value for the values (the multiple choice answers)
		for (var colourIndex in colours) { // for each available colour that has been fetched from the config
			var colour = colours[colourIndex]; // get the actual colour name
			var value; // define a variable for storing the colours value
			if (colour == answerColour) { // if the loop colour is the colour linked to the answer
				value = answer; // the value of this colour is the answer to the question
			}else{ // the loop colour is not that which is linked to the answer
				value = randIntInRange(answer-variance, answer+variance); // the value is a random integer complying to a set variance
				while (values.indexOf(value) > -1) { // while the value has already been chosen
					value = randIntInRange(answer-variance, answer+variance); // set a new value coplying to the same variance
				}
				values.push(value); // add this value to a values array to prevent duplicates
			}

			response.colours[colour] = {}; // define an empty dictionary for storing metadata about this colour
			response.colours[colour].value = value; // set the value of this colour

			response.colours[colour].locs = []; // define an empty array to store the food locations of this colour
			for (var i = 0; i < CONFIG.foodPerColour; i++) { // loop 'number of food items per colour' times
				var position = GAME.getRandCoord(); // set the position for this food item to a random coord
				while (position in response.colours[colour].locs) { // while there's already a food item in that position
					position = GAME.getRandCoord(); // get a new position for this food item
				}
				response.colours[colour].locs.push(position); // append this location to the array of locations for the loop colour
			}

		}

		GAME.question = response; // finally update the GAME question to store this generated question data.

	}

}


GAME.setup();


var snakes = {};
class Snake {
	constructor(snakeId, name, colour){
		this.id = snakeId;
		this.name = name;
		this.colour = colour;
		this.alive = true;
		this.score = 0;

		newLog("NEW_SNAKE id:" + this.id + " | name:" + this.name + " | colour:" + this.colour);

		var startPos;

		var error = true;
		while (error) {
			startPos = GAME.getRandCoord();
			this.body = [startPos];

			var direction = randIntInRange(0,3); // 0,1,2,3
			this.direction = direction;

			var tailLen = CONFIG.startLength-1; // -1 because tail doesn't include head

			if (this.direction==0 && startPos.y+tailLen <= CONFIG.gridMax && startPos.y-CONFIG.minStartDistance >= 0) { // north
				error=false;
				for (var i = 1; i <= tailLen; i++) {
					this.body.push({
						x:startPos.x,
						y:startPos.y+i
					});
				}

			}else if (this.direction==1 && startPos.x-tailLen >= 0 && startPos.x+CONFIG.minStartDistance <= CONFIG.gridMax) { // east
				error=false;
				for (var i = 1; i <= tailLen; i++) {
					this.body.push({
						x:startPos.x-i,
						y:startPos.y
					});
				}

			}else if (this.direction==2 && startPos.y-tailLen >= 0 && startPos.y+CONFIG.minStartDistance <= CONFIG.gridMax) { // south
				error=false;
				for (var i = 1; i <= tailLen; i++) {
					this.body.push({
						x:startPos.x,
						y:startPos.y-i
					});
				}

			}else if (this.direction==3 && startPos.x+tailLen <= CONFIG.gridMax && startPos.x-CONFIG.minStartDistance >= 0) { // west
				error=false;
				for (var i = 1; i <= tailLen; i++) {
					this.body.push({
						x:startPos.x+i,
						y:startPos.y
					});
				}

			}
		}

	}

	static validateUsername(username){
		for (var snakeId in snakes) {
			if (snakes[snakeId].name == username) {
				return "username_taken"
			}
		}

		if (username.length<3 || username.length>12) {
			return "invalid_length"
		}

		var regexPattern = /^[a-zA-Z0-9]+$/;
		if (!regexPattern.test(username)) {
			return "invalid_chars";
		}

		return false;
	}

	// used to change the direction of the snake to the given direction parameter
	changeDirection(newDir){
		var reverseDir = this.direction+2; // calculate the reverse of the snakes current direction
		if (reverseDir>3) {
			reverseDir -=4;
		}
		// if given direction is not going back on itself, and isn't the same as current direction
		if ((newDir != reverseDir) && (this.direction != newDir)) {
			this.direction = newDir; // save the snakes new direction
		}

	}

	eatFood(coord){ // called when a snake eats a food item on the grid
		var self = this; // create a reference for 'this' snake, so that it can be accessed without interruption
		GAME.foodAt(coord, function(food){ // get the food item at the given coordinate on the game canvas
			if (food) { // if there is food
				var loc = (GAME.question.colours[food.colour].locs).splice(food.index, 1); // removes item from array and returns its value
				if (food.colour == GAME.question.answerColour) { // eaten CORRECT answer
					self.addScore(2);
					self.increaseLength(2);
					GAME.newQuestion();

				}else{ // eaten WRONG answer
					self.removeScore(1);
					self.decreaseLength(1)

				}
			}
		});

	}

	addScore(amount){
		this.score += amount;
	}

	removeScore(amount){
		this.score -= amount;
		if (this.score < 1) { // ensure score can't go below 0
			this.score = 0;
		}
	}

	increaseLength(amount){ // increase the length of the snake
		for (var i = 0; i < amount; i++) {
			var newPos = {x:-1,y:-1}; // generate a new position that's not on the grid.
			// It will be put in place when the next frame is rendered.
			this.body.push(newPos); // append it to the snakes body
		}
	}
	decreaseLength(amount){ // decrease the length of the snake
		for (var i = 1; i <= amount; i++) {
			if ((this.body.length-amount) > 0) { // if the snake will still be atleast 1 in length
				this.body.pop();
			}
		}
	}



	die(){ // method to inform the client of the death, and delete this snake instance
		this.alive = false; // this snake instance is no longer alive
		io.emit('snakeDie', JSON.stringify(this)); // emit a snakeDie event to this client

		newLog("SNAKE_DIE id:" + this.id + " | name:" + this.name); // log snakes death to file

		delete snakes[this.id]; // delete the snake from the snakes object

	}

}




// returns a random integer in range (min,max) INCLUSIVE
function randIntInRange(min, max) {
	return Math.floor(Math.random() * (max-min +1)) + min;
}


var nextSnakeColourIndex = 0;
io.on('connection', function(client){ // when a client connections to the server
	var CLIENT_ID = client.id; // matches CLIENT_ID on client side.
	var TIMESTAMP = Date.now(); // get the current unix timestamp (milliseconds)

	// var logValue = "CLIENT_CONNECT {";
	// logValue += "\n  Id: " + CLIENT_ID;
	// logValue += "\n  Time: " + new Date(TIMESTAMP);
	// logValue += "\n  Timestamp: " + TIMESTAMP;

	newLog("CLIENT_CONNECT id:" + CLIENT_ID + " | timestamp:" + TIMESTAMP + " | time:" + new Date(TIMESTAMP))

	// newLog(logValue); // log the connection to file

	// when a getConfig event is emitted from the client
	client.on('getConfig', function(callback){ // callback parameter
		callback(CONFIG); // execute the callback function, giving the CONFIG object as a parameter
	});

	// when a newSnake event is emitted from the client
		// used when a new user joins the game, and so needs their snake added in
	client.on('newSnake', function(snakeData, callback){ // given data about the snake as a param
		// validate username
		var errorTxt = Snake.validateUsername(snakeData.name);
		if (!errorTxt) { // if no errors (the return false previously mentioned)
			var snakeColour = CONFIG.snakeColours[nextSnakeColourIndex]; //
			nextSnakeColourIndex = (nextSnakeColourIndex>=CONFIG.snakeColours.length-1 ? 0 : nextSnakeColourIndex+1);
			// instantate the Snake class with the snakes data...
			snakes[CLIENT_ID] = new Snake(CLIENT_ID, snakeData.name, snakeColour); // snakeID, name, colour
			errorTxt = "";
		}
		callback({error:errorTxt}); // callback to the client incase any errors occured

	});

	// when a changeDir'ection event is emitted from the client
		// given the new direction as a parameter. no callback is necessary as no data needs to be returned
	client.on('changeDir', function(newDir){
		if (typeof snakes[CLIENT_ID] !== 'undefined') { // if a snake with this ID exists
			snakes[CLIENT_ID].changeDirection(newDir); // run the instance method to change this snakes direction
		}
	});


	client.on('disconnect', function(reason){ // when the client disconnects from the web socket
		console.log("-CLIENT: " + client.id); // log their disconnection to the console

		delete snakes[CLIENT_ID]; // delete the snake from the snakes object list

	});

});
