
const socket = io.connect(); // setup the connection with the web socket server


// CONSTANTS
var CLIENT_ID; 	// set on socket.io connect.
var CONFIG;			// loaded from server on connection.

var gameReady = false; // game starts off not ready


$(window).on('resize', function(event){ // on browser window resize
  GAME.updateOffset(); // update the canvas offset on the users screen
});


$('#btn_play').on('click', function(ev){ // jQuery captures the button press event for the 'PLAY' btn
  var username = $('#usernameInput').val(); // get the value of the username input
  GAME.start(username, function(error){ // call start method of `GAME`, an instance of the `Game` class
    // called back due to error
    switch (error) {
      case 'invalid_length': // the username is of an invalid length
        $('#usernameError').text("Your username must be between 3 and 12 characters long!");
        break;

      case 'username_taken': // the username is already being used by another online user
        $('#usernameError').text("Sorry, someone is already using that username!");
        break;

      case 'invalid_chars':
        $('#usernameError').text("Only letters and numbers are allowed!");
        break;

      default: // default error message incase none of the above conditions are met.
        $('#usernameError').text("Invalid username!");
    }

  });

});

$('#btn_playAgain').on('click', function(ev){ // on click of the play again button
  GAME.showMenu('START'); // show the start menu
});



$(window).on('keydown', function(event){ // on press of keyboard key
	var newDir;

  switch (event.keyCode) { // switch the keycode of the pressed key
    case 65: // [a]
    case 37: // ArrowLeft:west;3
			newDir=3; // west
      break;
    case 87: // [w]
    case 38: // ArrowUp:north;0
			newDir=0; // north
      break;
    case 68: // [d]
    case 39: // ArrowRight:east;1
			newDir=1; // south
      break;
    case 83: // [s]
    case 40: // ArrowDown:south;2
			newDir=2; // east
      break;
  }

	if (newDir != null) { // if a direction change key was pressed
		socket.emit('changeDir', newDir); // emit a changeDir'ection event to the server, so that this users snake data can be updated
	}

});




socket.on('connect', function(){ // client establish connection with server.
	CLIENT_ID = socket.id; // store the clients id for future usage

  socket.emit('getConfig', function(response){ // load config object from server.
		CONFIG = response; // updte config constant to contain CONFIG object from server.
		// GAME.setup(); // necessary details loaded from server, so setup the GAME ready for the user.
    GAME = new Game($('#GAME')[0], CONFIG.canvasSize, CONFIG.canvasSize);
  });
});

socket.on('renderFrame', function(snakes, question){
	if (Object.keys(snakes).length > 0) { // atleast 1 snake to be rendered.
		GAME.render(snakes, question);
		if (typeof snakes[CLIENT_ID] !== 'undefined') { // the client has a snake that exists in the 'snakes' array
			GAME.updateOffset(snakes[CLIENT_ID].body[0]);
		}

	}else{ // shouldn't ever reach this as server only sends a frame if atleast 1 snake exists
		console.log('no_snakes');
	}

});

socket.on('snakeDie', function(snake){
  snake = JSON.parse(snake);
  if (snake.id == CLIENT_ID) {
    var data = {'finalScore':snake.score};
    GAME.showMenu('END', data);
  }
});




var GAME; // will be used to store the single instance of the `Game` class
class Game {
  // initialise the class, given 3 parameters..
    // canvas: the canvas element, in the HTML document, that will be used for the game
    // canvasWidth: the width of the game canvas
    // canvasHeight: the height of the game canvas
  constructor(canvas, canvasWidth, canvasHeight){
    this.canvas = canvas;

    this.canvas.width = canvasWidth;
    this.canvas.height = canvasHeight;
    this.updateOffset(); // update the canvas offset now that the correct widths and heights have been set.

    this.context = this.canvas.getContext("2d"); // get the context for the game canvas
    this.frameNo = 0; // frame counter starts at 0
    this.leaderboard = {}; // a dictionary linking leaderboard position to key:value objects of each ingame user and their scores
  }


  updateOffset(coords = null) { // updates the canvas offset in clients browser window
    var offsetWidth = document.body.offsetWidth,
        offsetHeight = document.body.offsetHeight;

    if (coords) {
      this.canvas.style.left  = (-(coords.x*20) + offsetWidth/2) + 'px';
      this.canvas.style.top   = (-(coords.y*20) + offsetHeight/2) + 'px';

    }else{ // coords not sent as a param so just center canvas on screen
      this.canvas.style.left  = -((CONFIG.canvasSize-offsetWidth)/2) + 'px';
      this.canvas.style.top   = -((CONFIG.canvasSize-offsetHeight)/2) + 'px';
    }

  }


  start(username, errorCall){ // on game start
		var snakeData = { // using a dictionary incase more data is added in the future
			name: username
		};

		socket.emit('newSnake', snakeData, function(response){ // emit a newSnake event to the backend
			if (response.error) { // if there is an error with creating the new snake
        errorCall(response.error); // call an error on the game start execution
			}else{ // no error, game can start
				gameReady = true; // game is now ready

        GAME.showMenu("NONE"); // hide menus

        $('section#leaderboard').css('display','block'); // unhide the leaderboard
        $('section#score').css('display','block'); // unhide the score display

			}
		});

  }

  render(snakes, question) { // recieved from backend when a new game frame is ready to be rendered
    if (gameReady) { // if the game is ready
      CANVAS.clear(); // clear the canvas, ready for new game frame

      // Question Setup
      $('#question').text(question.question + '= '); // set the question overlay elements content to the new question
      $('#answerChoices').text(''); // clear the currentr answer choices
      for (var colour in question.colours) { // for each question answer color
        var choiceEl = '<span style="color: ' + colour + '">' + question.colours[colour].value + '</span>'
        $('#answerChoices').append(choiceEl); // add the choice to the users list of available answers for this question

        var locations = question.colours[colour].locs;
        for (var loc of locations) { // for each food item location
          CANVAS.addSquare(loc.x, loc.y, colour); // add a food item to the grid at [loc.x, loc.y]
        }
      }


      // Snakes Setup
      var newLeaderboard = {};
      var lbPos = 0; // leaderboard position (temp positions until sorted later on)
      for (var snakeId in snakes){ // for each alive snake
        var self = snakes[snakeId];
        newLeaderboard[lbPos] = {"name": self.name, "score":self.score}; // setup random ordered leaderboard for now
        lbPos++;
        for (var i = 0; i < self.body.length; i++) { // for each part of this loop-snakes body
          var bodyX = self.body[i].x;
          var bodyY = self.body[i].y;

          CANVAS.addSquare(bodyX, bodyY, self.colour); // add the square for this body part to the grid
        }

        if (snakeId == CLIENT_ID) { // if the snake is the clients snake, adjust screen to center the snakes head on screen
          GAME.updateOffset(); // update the game canvas offset on the users screen
          $('#playerScore').text(self.score); // update score counter
        }

      }

      if (GAME.frameNo%10 == 0) { // update only when the remainder of frameNo/10 is 0 (so every 10 frames)
        GAME.updateLeaderboard(newLeaderboard); // update the leaderboard with the new users
      }

      GAME.frameNo++; // increment the GAME's frame number
    }

  }


  updateLeaderboard(newLb){ // newLb -> newLeaderboard

    // BUBBLE SORT: to sort the leaderboard
    var swap = true;
    while (swap) { // while a swap has occurred
      swap = false; // default to no swap
      for (var i = 0; i < Object.keys(newLb).length-1; i++) { // for each user in the leaderboard
        if (newLb[i].score < newLb[i+1].score) { // if the loop-users score is less than the next users score
          swap = true; // a swap needs to happen
          var temp = newLb[i]; // temporarily store loop-users score
          newLb[i] = newLb[i+1]; // set loop-users score to next users score
          newLb[i+1] = temp; // set the next users score to the temp saved score
        }
      }
    }

    var lbContent = "";
    for (var index in newLb) { // for each user in the sorted leaderboard
      var record = newLb[index];
      var position = parseInt(index)+1;
      lbContent += '<li>' + position + '. ' + record.name + ' <span class="lb-score">' + record.score + '</span></li>';
    }
    $('#leaderboard ul').html(lbContent); // add the leaderboard back to the game document

  }


  showMenu(menuId, data){ // show a menu overlay to the user

    if (menuId == "NONE") {
      $('section#menus').fadeOut();

    }else{

      if (menuId == "START") {
        $('#menu_end').fadeOut(0);
        $('#menu_start').fadeIn(0);

      }else if (menuId == "END") {
        $('#menu_start').fadeOut(0);
        $('#menu_end').fadeIn(0);

        $('#final_score').text(data.finalScore);

      }

      $('section#menus').fadeIn();
    }

  }

}




var CANVAS = { // CANVAS variable, stores data about the game canvas displayed to the user
	canvas : $('#GAME')[0], // gets the game canvas HTML document element

  clear: function(){ // function to clear the cdanvas
    GAME.context.clearRect(0, 0, CONFIG.canvasSize, CONFIG.canvasSize); // draws a cleare rectangle filling the canvas
  },

  getCoord: function(coord){ // convert a minified coordinate to it's full location
    var newCoord = (coord*20)+1; // multiply by cell width, add 1
    return newCoord;
  },

	getRandCoord: function(){ // get a random position on the canvas
    // random number between 0,1, multiplied by the max possible value, rounded to 0 decimal places (integer)
		var randX = (Math.random()*CONFIG.gridMax).toFixed(0);
		var randY = (Math.random()*CONFIG.gridMax).toFixed(0);

		return {x:randX,y:randY}; // return the coordinate pair
	},

  addSquare: function(xCoord, yCoord, style='white'){ // add a square to the grid with given options (location and style)
    var newX = CANVAS.getCoord(xCoord), newY = CANVAS.getCoord(yCoord);

    var ctx = GAME.context;
    ctx.fillStyle = style;

    ctx.beginPath(); // begin drawing a path
      ctx.rect(newX, newY, 18, 18); // draw a new rectangle (square) onto the grid
    ctx.fill(); // fill the square with the given colour
  },

  delSquare: function(xCoord, yCoord){ // remove a square from the grid.  e.g: eating food
    var newX = CANVAS.getCoord(xCoord), newY = CANVAS.getCoord(yCoord);

    var ctx = GAME.context;
    ctx.clearRect(newX, newY, 18, 18);
  }

}
