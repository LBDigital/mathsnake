# MathSnake

A 'Snake' based online multiplayer game, built using NodeJS, to help younger students improve their mathematical ability.

Control your snake with arrow keys (or WASD). A question will be shown at the top of your screen, and you must eat the correctly coloured food to match the correct answer at the bottom of your screen. You gain 2 points for eating correctly coloured blocks and lose a point for eating the wrong colour.

---
This Node app has the following NGINX configuration file to reverse proxy the node app on port :1337 to HTTP port :80

```
server {
        listen 80;
        server_name 178.62.14.106;

        location / {
                proxy_pass              "http://localhost:1337";
                proxy_http_version      1.1;
                proxy_set_header        Upgrade $http_upgrade;
                proxy_set_header        Connection "Upgrade";
                proxy_set_header        Host $host;
        }
}
```